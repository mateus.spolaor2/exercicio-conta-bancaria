package com.mastertech.exercicio;

public class Cliente {

    private Integer idade;
    private String nome;
    private Conta conta;

    public Cliente(){}

    public Cliente(Integer idade, String nome) {
        this.idade = idade;
        this.nome = nome;
    }

    public Conta getConta() {
        return conta;
    }

    public void setConta(Conta conta) {
        this.conta = conta;
    }

    public Integer getIdade() {
        return idade;
    }

    public void setIdade(Integer idade) {
        this.idade = idade;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Boolean ehMaiorDeIdade() {
        if(null != this.idade && this.idade >= 18){
            return true;
        } else {
            return false;
        }
    }
}
