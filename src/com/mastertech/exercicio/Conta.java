package com.mastertech.exercicio;

public class Conta {

    private Double saldo;

    public Conta() {
    }

    public Conta(Double saldo) {
        this.saldo = saldo;
    }

    public Double getSaldo() {
        return saldo;
    }

    public void setSaldo(Double saldo) {
        this.saldo = saldo;
    }
}
