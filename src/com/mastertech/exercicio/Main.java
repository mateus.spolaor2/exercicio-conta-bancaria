package com.mastertech.exercicio;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Atm atm = new Atm(true);
        System.out.println("Bem vindo ao sistema de conta bancária do Mateus"
                + "\n");


        Cliente cliente = atm.cadastraCliente();

        while (atm.getSistemaOnline()) {
            atm.abrirMenu(cliente);
        }
    }
}
