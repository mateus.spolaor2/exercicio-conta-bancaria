package com.mastertech.exercicio;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Atm {

    Boolean sistemaOnline;

    public Atm(Boolean sistemaOnline) {
        this.sistemaOnline = sistemaOnline;
    }

    public Boolean getSistemaOnline() {
        return sistemaOnline;
    }

    public void setSistemaOnline(Boolean sistemaOnline) {
        this.sistemaOnline = sistemaOnline;
    }

    Scanner scannerNumber = new Scanner(System.in);
    Scanner scannerStrings = new Scanner(System.in);

    public Cliente cadastraCliente() {

        String clienteValido = "N";

        while (clienteValido.equals("N")) {
            Cliente cliente = insereDadosCliente();

            if(cliente.ehMaiorDeIdade()){
                Conta conta = new Conta (0D);
                cliente.setConta(conta);
                return cliente;
            } else {
                System.out.println("Cadastro não realizado. Cliente menor de idade. \n" +
                        "Deseja realizar o cadastro novamente? (S/N)");

                String continuar = scannerStrings.nextLine();

                if(continuar.equalsIgnoreCase("N")){
                    System.out.println("Até logo!");
                    System.exit(1);
                }
            }
        }
        return null;
    }

    private Cliente insereDadosCliente() {
        String nome;
        Integer idade;
        Cliente cliente = new Cliente();

        System.out.println("Insira seu nome: ");
        nome = scannerStrings.nextLine();
        cliente.setNome(nome);
        System.out.println("Nome: " + nome);

        System.out.println("Insira sua idade: ");
        idade = scannerNumber.nextInt();
        cliente.setIdade(idade);
        System.out.println("Idade: " + idade);

        return cliente;
    }

    public void abrirMenu(Cliente cliente) {

        System.out.println(
                "---------- \n" +
                "Opções: \n" +
                "1 - Saque \n" +
                "2 - Depósito \n" +
                "3 - Consulta de Saldo \n" +
                "4 - Sair do sistema"
        );

        int opcaoMenu = scannerNumber.nextInt();
        Conta conta = cliente.getConta();

        executaOpcoes(opcaoMenu, conta);

    }

    private void executaOpcoes(int opcaoMenu, Conta conta) {

        if(opcaoMenu == 1) {
            sacarDinheiro(conta);
        } else if (opcaoMenu == 2){
            depositarDinheiro(conta);
        } else if (opcaoMenu == 3) {
            exibeSaldo(conta);
        }

        else {
            System.out.println("Obrigado por utilizar os serviços do Mateuzinho =)");
            this.sistemaOnline = false;
        }
    }

    private void exibeSaldo(Conta conta) {
        System.out.println("Seu saldo é: " + saldoEmReais(conta));
    }

    private void depositarDinheiro(Conta conta) {
        System.out.println(
                "Opção selecionada: depósito. \n" +
                        "Qual valor você deseja depositar?"
        );

        double valorDeposito = scannerNumber.nextDouble();
        conta.setSaldo(conta.getSaldo() + valorDeposito);
        System.out.println("Seu saldo atual é de: " +  saldoEmReais(conta));
    }

    private void sacarDinheiro(Conta conta) {
        System.out.println(
                "Opção selecionada: saque. \n" +
                "Qual valor você deseja sacar?"
        );

        double valorSaque = scannerNumber.nextDouble();
        conta.setSaldo(conta.getSaldo() - valorSaque);
        System.out.println("Seu saldo atual é de: " + saldoEmReais(conta));
    }


    private String saldoEmReais(Conta conta) {
        DecimalFormat decimalFormat = new DecimalFormat("R$" + "#,##0.00");
        return decimalFormat.format(conta.getSaldo());
    }
}